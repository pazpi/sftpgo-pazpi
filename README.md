# sftpgo-pazpi

Custom made sftpgo Dockerfile for my need

## Install instruction

Download the release of sftpgo necessary, the one used is the `0.9.6`

[Official releases](https://github.com/drakkan/sftpgo/releases/)

Put the `.tar.xz` alongside the `Dockerfile` and issue the command

```
podman build -t myname/sftpgo .
```

This will build the container locally as `myname/sftpgo`

Feel free to change `myname` as you like

In order this program to work it needs three folder, `config`, `data`, `backups`.
Create this folder (`mkdir {config,data,backups}`) and than create initialize sftpgo
which in our case a SQLite database is more that enough.

```
podman run --rm -v $HOME/sftpgo-podman/config:/app/config pazpi/sftpgo initprovider -c /app/config
```

The final step is to create and start the container

```
podman create --name sftpgo \
-p 8080:8080 \
-p 2022:2022 \
-v $HOME/sftpgo/config:/app/config \
-v $HOME/sftpgo/data:/app/data \
-v $HOME/sftpgo/backups:/app/backups \
myname/sftpgo
```

Extra directory can be added by inserting new volumes under `/app/data` as `-v /my/path:/app/data/mypath`