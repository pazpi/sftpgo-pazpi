FROM debian:latest as buildenv
ADD sftpgo_v1.0.0_linux_x86_64.tar.xz /build


FROM debian:latest
ARG BASE_DIR=/app
ARG DATA_REL_DIR=data
ARG CONFIG_REL_DIR=config
ARG BACKUP_REL_DIR=backups
ARG WEB_REL_PATH=web

ENV HOME_DIR=${BASE_DIR}/${USERNAME}
ENV DATA_DIR=${BASE_DIR}/${DATA_REL_DIR}
ENV CONFIG_DIR=${BASE_DIR}/${CONFIG_REL_DIR}
ENV BACKUPS_DIR=${BASE_DIR}/${BACKUP_REL_DIR}
ENV WEB_DIR=${BASE_DIR}/${WEB_REL_PATH}

RUN mkdir -p ${DATA_DIR} ${CONFIG_DIR} ${WEB_DIR} ${BACKUPS_DIR}

COPY --from=buildenv /build/sftpgo /bin/sftpgo
COPY --from=buildenv /build/templates ${WEB_DIR}/templates
COPY --from=buildenv /build/static ${WEB_DIR}/static

EXPOSE 2022 8080

# the defined volumes must have write access for the UID and GID defined above
VOLUME [ "$DATA_DIR", "$CONFIG_DIR", "$BACKUPS_DIR" ]

# override some default configuration options using env vars
ENV SFTPGO_CONFIG_DIR=${CONFIG_DIR}
# setting SFTPGO_LOG_FILE_PATH to an empty string will log to stdout
ENV SFTPGO_LOG_FILE_PATH=""
ENV SFTPGO_HTTPD__BIND_ADDRESS=""
ENV SFTPGO_HTTPD__TEMPLATES_PATH=${WEB_DIR}/templates
ENV SFTPGO_HTTPD__STATIC_FILES_PATH=${WEB_DIR}/static
ENV SFTPGO_DATA_PROVIDER__USERS_BASE_DIR=${DATA_DIR}
ENV SFTPGO_HTTPD__BACKUPS_PATH=${BACKUPS_DIR}

ENTRYPOINT ["sftpgo"]
CMD ["serve"]
